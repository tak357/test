<script>

// サロン名の最小幅
const LOGO_MIN_WIDTH = 245;

function logoWidthSet() {
  // windowの左側のoffsetも取得してうんたらかんたら
  var menuOffset = $("#mainnavi").offset();
  // 左右のpadding分（10px+10px）を引く
  var logoWidth = (menuOffset.left - 20);

  if (logoWidth >= LOGO_MIN_WIDTH) {
    $("#logo").css("width", logoWidth);
    $("#logo > h1").css("width", logoWidth);
  } else {
    $("#logo").css("width", LOGO_MIN_WIDTH);
    $("#logo > h1").css("width", LOGO_MIN_WIDTH);
  }
  $("#logo > h1").html(salonName);
}
</script>

@media screen and (max-width:500px){
  #logo h1 {
    width: 245px !important;
  }
}